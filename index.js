let words,
    filteredWords,
    activeWords = JSON.parse(localStorage.getItem('activeWords')) || [];

let isActive = (id) => {
    return activeWords.includes(id);
}

let getRow = (index, word) => {
    return `<input type="checkbox" 
        class="word_checkbox"
        id="check_${word.id}" 
        ${isActive(word.id) ? 'checked' : ''} 
        data-id="${word.id}">
            <label class="row word" for="check_${word.id}">
                <div class="col-sm-2 col-md-1 index">${index}</div>
                <div class="col-sm-4 col-md-5">${word.word}</div>
                <div class="col-sm-4 col-md-5">${word.add || ''}</div>
                <div class="col-sm-2 col-md-1 level">${word.level}</div>
            </label>`;
}

let compare = (a, b, field, dir) => {
    if(a[field].toLowerCase() < b[field].toLowerCase()) return -1 * dir;
    if(a[field].toLowerCase() > b[field].toLowerCase()) return 1 * dir;
    return 0;
}

let loadWords = () => {
    fetch('words.json')
        .then(response =>
            response.json()
        )
        .then(data => {
            words = data;
            filterWords();
            sortWords();
            redrawWords();
        })
        .catch(err =>
            console.log(err)
        );
}

let filterWords = () => {
    let filter = document.getElementById('search').value,
        hide = document.getElementById('hide_known').checked,
        activeLevel = document.getElementById('select-level').value;
    filteredWords = words.filter(word => {
        return word.word.includes(filter) &&
            (activeLevel === '' || activeLevel === word.level) &&
            !(hide && isActive(word.id))
    })
}

let sortWords = () => {
    let activeOrder = document.getElementById('select-order').value;
    return filteredWords.sort((a, b) => {
        switch (activeOrder) {
            case 'nameDesc':
                return compare(a, b, 'word', 1);
            case 'nameAsc':
                return compare(a, b, 'word', -1);
            case 'levelDesc':
                return compare(a, b, 'level', 1);
            case 'levelAsc':
                return compare(a, b, 'level', -1);
        }
    });
}

let redrawWords = () => {
    let text = `<div class="row word">
        <div class="col-sm-2 col-md-1 index"></div>
        <div class="col-sm-8 col-md-10">Words count</div>
        <div class="col-sm-2 col-md-1 level">${filteredWords.length}</div>
      </div>`;
    document.getElementById('words').innerHTML = text;
    let index = 1;
    for (let word of filteredWords) {
        text += getRow(index, word);
        index ++;
    }
    document.getElementById('words').innerHTML = text;
    document.documentElement.scrollTop = 0;
    updateCounter();
}

let updateCounter = () => {
    let count = document.querySelectorAll('.word_checkbox:checked').length;
    document.getElementById('checked').innerText = count;
    document.getElementById('all').innerText = filteredWords.length;
}

let keyUp = e => {
    if(e.keyCode === 13) {
        filterWords();
        sortWords();
        redrawWords();
    }
}

let selectLevel = () => {
    filterWords();
    sortWords();
    redrawWords();
}

let select_order = () => {
    sortWords();
    redrawWords();
}

let checkWord = e => {
    let el = e.target,
        id = parseInt(el.dataset.id);

    if (el.localName === 'input') {
        if (activeWords.includes(id)) {
            let index = activeWords.indexOf(id);
            activeWords.splice(index, 1);
        } else {
            activeWords.push(id);
        }
        localStorage.setItem('activeWords', JSON.stringify(activeWords));
        updateCounter();
    }
}

let toggleKnown = () => {
    filterWords();
    sortWords();
    redrawWords();
}

document.addEventListener('DOMContentLoaded', loadWords);
document.getElementById('search').addEventListener('keyup', keyUp);
document.getElementById('select-level').addEventListener('change', selectLevel);
document.getElementById('select-order').addEventListener('change', select_order);
document.getElementById('words').addEventListener('click', checkWord);
document.getElementById('hide_known').addEventListener('change', toggleKnown);

